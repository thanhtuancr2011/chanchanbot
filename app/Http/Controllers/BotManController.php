<?php

namespace App\Http\Controllers;

use App\FacebookUser;
use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
use App\Conversations\ExampleConversation;

class BotManController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }

    /**
     * Place your BotMan logic here.
     */
    public function handle(Request $request)
    {
        //  Get response data
        $data = $request->all();

        // Format response data
        $response = formatResponseData($data);
        $senderId = $response['senderId'];
        $messageText = $response['messageText'];

        switch (strtolower($messageText)) {

            case SAVE_INFO:
                
                $user = FacebookUser::where('facebook_id', $senderId)->first();
                if (!$user) {
                    $userData = getUserProfile($senderId);
                    if (isset($userData['id'])) {
                        $userData['facebook_id'] = $userData['id']; 
                        unset($userData['id']);
                    }
                    $user = FacebookUser::create($userData);
                }

                break;
            
            default:
                # code...
                break;
        }
    }

    public function sendMessageTest()
    {
        $recipientId = 2603415253008682;
        $messagePayload = [
            "template_type" => "generic",
            "image_aspect_ratio" => "SQUARE",
            "elements" => [
                [
                    "title" => "Ronan Nguyen, 38",
                    "image_url" => "https://images.chatfuel.com/bot/raw/aa2f1641-04f4-43bb-a05a-e0502968b0b3",
                    "subtitle" => "H: Thanh Xuân • W: Hoàn Kiếm, Hà nội"
                ],
                [
                    "title" => "Ronan Nguyen, 38",
                    "image_url" => "https://images.chatfuel.com/bot/raw/aa2f1641-04f4-43bb-a05a-e0502968b0b3",
                    "subtitle" => "H: Thanh Xuân • W: Hoàn Kiếm, Hà nội"
                ]
            ]
        ];

        $quickReplies = [
            [
                "content_type" => "text",
                "title" => "Siêu thích",
                "payload" => "tuanit",
            ],
            [
                "content_type" => "text",
                "title" => "Thích",
                "payload" => "tuanit",
            ],
            [
                "content_type" => "text",
                "title" => "Không thích",
                "payload" => "tuanit",
            ]
        ];

        // Send mesage
        $response = sendMessageTemplateAndReply($recipientId, $messagePayload, $quickReplies);
        dd($response);
    }
}
