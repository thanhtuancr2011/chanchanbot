<?php

    // Actions
    define("SAVE_INFO", "save info");

    // Urls
    define("FACEBOOK_GRAPH_URL", "https://graph.facebook.com/");
    define("PAGE_ACCESS_TOKEN", env("FACEBOOK_TOKEN"));
    define("MESSAGE_API_URL", FACEBOOK_GRAPH_URL . "v2.6/me/messages?access_token=" . PAGE_ACCESS_TOKEN);
    define("PROFILE_API_URL", FACEBOOK_GRAPH_URL . "<PSID>?fields=first_name,last_name,gender,profile_pic&access_token=" . PAGE_ACCESS_TOKEN);

?>