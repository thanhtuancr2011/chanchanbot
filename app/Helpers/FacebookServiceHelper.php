<?php

    function formatResponseData($data)
    {
        // Default value
        $senderId = null;
        $messageText = null;

        // Get sender id
        if (
            isset($data['entry']) && 
            isset($data['entry'][0]) && 
            isset($data['entry'][0]['messaging']) && 
            isset($data['entry'][0]['messaging'][0])
        ) {
            $senderId = isset($data['entry'][0]['messaging'][0]['sender']) ? 
                        $data['entry'][0]['messaging'][0]['sender']['id'] : '';
            $messageText = isset($data['entry'][0]['messaging'][0]['message']) && isset($data['entry'][0]['messaging'][0]['message']['text']) ? 
                        $data['entry'][0]['messaging'][0]['message']['text'] : '';
        }

        return [
            "senderId" => $senderId,
            "messageText" => $messageText
        ];
    }

    function getUserProfile($id)
    {
        $content = curlGet(str_replace('<PSID>', $id, PROFILE_API_URL));
        \Log::info("GetUserProfile: " . $content);

        return json_decode($content, TRUE);
    }

    function sendMessageTemplateAndReply($recipientId, $messagePayload, $quickReplies)
    {
        $messageData = [
            "recipient" => [
                "id" => $recipientId
            ],
            "messaging_type" =>  "RESPONSE",
            "message" => [
                "attachment" => [
                    "type" => "template",
                    "payload" => $messagePayload
                ],
                "quick_replies" => $quickReplies
            ]
        ];

        $content = curlPost(MESSAGE_API_URL, $messageData);
        \Log::info("SendMessageTemplateAndReply: " . $content);

        return json_decode($content, TRUE);
    }

    function sendTextMessage($recipientId, $messageText)
    {
        $messageData = [
            "recipient" => [
                "id" => $recipientId
            ],
            "message" => [
                "text" => $messageText
            ]
        ];

        $content = curlPost(MESSAGE_API_URL, $messageData);
        \Log::info("SendTextMessage: " . $content);

        return json_decode($content, TRUE);
    }
?>